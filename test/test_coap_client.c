
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <windows.h>
#include <coap.h>

#include <unity_fixture.h>

#define BUF_SIZE        1024
#define PORT            5683
#define MSG_BUF_LEN 64                  // coap 默认端口

int fd;                                 // 套接字句柄
struct sockaddr_in server_addr, client_addr;

TEST_GROUP(tg_coap_client);

TEST_SETUP(tg_coap_client)
{
    WSADATA wsaData;

    // Winsows下启用socket
    WSAStartup(MAKEWORD(2, 2), &wsaData);

    // 创建UDP套接字
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    server_addr.sin_family = AF_INET;
    //server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    server_addr.sin_port = htons(PORT);
    memset(&(server_addr.sin_zero),0, sizeof(server_addr.sin_zero));
}

TEST_TEAR_DOWN(tg_coap_client)
{
    closesocket(fd);
    WSACleanup();
}

TEST(tg_coap_client, tc01_sprintf)
{
    char str[32];
    sprintf(str, "Hello %s\n", "CoAP");
    TEST_ASSERT_EQUAL_STRING("Hello CoAP\n", str);
}

TEST(tg_coap_client, tc01_get_hello)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_GET);
    coap_set_mid(&msg_send, 0x6879);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"hello", 5);

    // 第五步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第六步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第七步，检查CoAP响应
    TEST_ASSERT_EQUAL_INT(CE_NONE, coap_validate_pkt(&msg_recv));
    TEST_ASSERT_EQUAL_UINT16(coap_get_mid(&msg_send), coap_get_mid(&msg_recv));

    // 检查响应是不是Hello CoAP
    coap_payload payload = coap_get_payload(&msg_recv);
    TEST_ASSERT_EQUAL_STRING_LEN("Hello CoAP", payload.val, payload.len);
}

TEST_GROUP_RUNNER(tg_coap_client)
{
    RUN_TEST_CASE(tg_coap_client, tc01_sprintf);
    RUN_TEST_CASE(tg_coap_client, tc01_get_hello);
}
