#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <windows.h>

#include <coap.h>

#define BUF_SIZE        1024 
#define PORT            5683
#define MSG_BUF_LEN 64                  // coap 默认端口

int fd;                                 // 套接字句柄
struct sockaddr_in server_addr, client_addr;

static void dump_pakcet(uint8_t *buf, int len)
{
    printf("Dump:\n");
    for (int i = 0; i < len; i++)
    {
        printf("%02X ", *buf++);
    }
    printf("\n");
}

static void payload_to_string(char *buf, size_t len, const coap_payload_t payload)
{
    memcpy(buf, payload.val, payload.len);
    buf[payload.len] = 0;
}

/**
 * @brief Get hello
 */
void test_get_hello(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_GET);
    coap_set_mid(&msg_send, 0x6879);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"hello", 5);

    // 第五步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第六步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第七步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第八步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            char str[32];
            payload_to_string(str, 32, payload);
            printf("CoAP Response Payload:\n");
            printf("%s\n", str);
            printf("\n");
        }
    }
}

void test_get_counter(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_GET);
    coap_set_mid(&msg_send, 0x0001);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"counter", 7);

    // 第五步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第六步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第七步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第八步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            char str[32];
            payload_to_string(str, 32, payload);
            printf("CoAP Response Payload:\n");
            printf("%s\n", str);
            printf("\n");
        }
    }
}

void test_get_json_counter(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_GET);
    coap_set_mid(&msg_send, 0xC133);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"json", strlen("json"));
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"counter", strlen("counter"));

    // 第五步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第六步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第七步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第八步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            char str[32];
            payload_to_string(str, 32, payload);
            printf("CoAP Response Payload:\n");
            printf("%s\n", str);
            printf("\n");
        }
    }
}

void test_get_add(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_GET);
    coap_set_mid(&msg_send, 0xC13F);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"add", strlen("add"));
    coap_add_option(&msg_send, CON_URI_QUERY, (uint8_t*)"a=2", strlen("a=2"));
    coap_add_option(&msg_send, CON_URI_QUERY, (uint8_t*)"b=3", strlen("b=3"));

    // 第五步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第六步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第七步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第八步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            char str[32];
            payload_to_string(str, 32, payload);
            printf("CoAP Response Payload:\n");
            printf("%s\n", str);
            printf("\n");
        }
    }
}

void test_put_add(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_PUT);
    coap_set_mid(&msg_send, 0xC1BF);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"add", strlen("add"));

    // 第五步，设置CoAP Payload
    coap_set_payload(&msg_send, (uint8_t*)"a=2&b=3", strlen("a=2&b=3"));

    // 第六步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第七步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第八步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第九步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            char str[32];
            payload_to_string(str, 32, payload);
            printf("CoAP Response Payload:\n");
            printf("%s\n", str);
            printf("\n");
        }
    }
}

void test_get_stream(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_GET);
    coap_set_mid(&msg_send, 0xB13F);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"stream", strlen("stream"));

    // 第五步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第六步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第七步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第八步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            dump_pakcet((uint8_t *)payload.val, payload.len);
            printf("\n");
        }
    }
}

void test_post_swap(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_POST);
    coap_set_mid(&msg_send, 0xC1BF);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"swap", strlen("swap"));

    // 第五步，设置CoAP Payload
    char str[] = {0x32,0x43,0x54};
    coap_set_payload(&msg_send, (uint8_t*)str, strlen(str));

    // 第六步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第七步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第八步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第九步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            dump_pakcet((uint8_t *)payload.val, payload.len);
            printf("\n");
        }
    }
}

void test_post_sort(void)
{
    // 第一步，声明发送Buf和接收Buf，最大长度均为64字节
    uint8_t msg_send_buf[MSG_BUF_LEN];
    coap_pdu_t msg_send = {msg_send_buf, 0, 64};
    uint8_t msg_recv_buf[MSG_BUF_LEN];
    coap_pdu_t msg_recv = {msg_recv_buf, 0, 64};

    // 第二步，辅助步骤，初始化发送Buf
    coap_init_pdu(&msg_send);

    // 第三步，设置CoAP Header部分四字节
    coap_set_version(&msg_send, COAP_V1);
    coap_set_type(&msg_send, CT_CON);
    coap_set_code(&msg_send, CC_POST);
    coap_set_mid(&msg_send, 0xD1BF);            // Message ID 任意

    // 第四步，加入路由 Uri-Path
    coap_add_option(&msg_send, CON_URI_PATH, (uint8_t*)"sort", strlen("sort"));

    // 第五步，设置CoAP Payload
    char str[] = {0x12,0x45,0x32};
    coap_set_payload(&msg_send, (uint8_t*)str, strlen(str));

    // 第六步，通过Socket发送CoAP请求，并等待必要的时间
    sendto(fd, (char*)msg_send.buf, msg_send.len, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));
    Sleep(500); // 必要的延时

    // 第七步，通过套接字接收CoAP响应
    int addr_len = sizeof(struct sockaddr);
    int bytes_recv = recvfrom(fd, (char *)msg_recv.buf, msg_recv.max, 0, (struct sockaddr *)&client_addr, &addr_len);
    msg_recv.len = bytes_recv;

    // 第八步，检查CoAP响应
    if (coap_validate_pkt(&msg_recv) == CE_NONE)
    {
        printf("Got Valid CoAP Packet\n");
        dump_pakcet((uint8_t *)msg_recv.buf, msg_recv.len);

        if (coap_get_mid(&msg_recv) == coap_get_mid(&msg_send))
        {
            // 第九步，获得响应并打印输出
            coap_payload_t payload = coap_get_payload(&msg_recv);
            dump_pakcet((uint8_t *)payload.val, payload.len);
            printf("\n");
        }
    }
}

int main(int argc, char **argv)
{
    WSADATA wsaData;
    int ret;

    // Winsows下启用socket
    ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (ret != 0) 
    {
        printf("WSAStartup failed: %d\n", ret);
        return 1;
    }

    // 创建UDP套接字
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    server_addr.sin_family = AF_INET;
    //server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    server_addr.sin_port = htons(PORT);
    memset(&(server_addr.sin_zero),0, sizeof(server_addr.sin_zero));
    
    printf("UDP Client %d\n", PORT);

    test_get_hello();
    test_get_counter();
    test_get_json_counter();
    test_get_add();
    test_put_add();
    test_get_stream();
    test_post_swap();
    test_post_sort();

    // 关闭套接字
    closesocket(fd);
    WSACleanup();

    // 输入任何字符则关闭程序
    printf("Finish\n");
    return 0;
}
