#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <windows.h>

#define BUF_SIZE        1024 
#define PORT            5683            // coap 默认端口
char recv_data[BUF_SIZE];               // 接收缓冲区
char send_data[BUF_SIZE];               // 发送缓冲区

int fd;
struct sockaddr_in server_addr, client_addr;

void dump_pakcet(uint8_t *buf, int len)
{
    printf("Dump:\n");
    for (int i = 0; i < len; i++)
    {
        printf("%02X ", *buf++);
    }
    printf("\n");
}

void test_simple(void)
{
    char buff[] = {0x40, 0x01, 0x59, 0x68, 0xb5, 'h', 'e', 'l', 'l', 'o'};
    sendto(fd, buff, 10, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));

    Sleep(500); // 必要的延时

    int addr_len = sizeof(struct sockaddr);
    int recv_len = recvfrom(fd, recv_data, BUF_SIZE - 1, 0,
                             (struct sockaddr *)&client_addr, &addr_len);
    recv_data[recv_len] = '\0';

    dump_pakcet((uint8_t *)recv_data, recv_len);
}

int main(int argc, char **argv)
{
    WSADATA wsaData;
    
    int ret;


    // Winsows下启用socket
    ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (ret != 0) 
    {
        printf("WSAStartup failed: %d\n", ret);
        return 1;
    }

    // 创建UDP套接字
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    server_addr.sin_family = AF_INET;
    //server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    server_addr.sin_port = htons(PORT);
    memset(&(server_addr.sin_zero),0, sizeof(server_addr.sin_zero));
    
    printf("UDP Client %d\n", PORT);

    test_simple();

    // 关闭套接字
    closesocket(fd);
    WSACleanup();

    // 输入任何字符则关闭程序
    printf("Finish\n");
    return 0;
}
